﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Button = System.Windows.Controls.Button;
using MessageBox = System.Windows.Forms.MessageBox;

namespace TicTacToe
{
    public partial class MainWindow : Window
    {
      
        private MarkType[] mResults;
        private bool mPlayer1Turn;
        private bool mGameEnded;

        public MainWindow()
        {
            InitializeComponent();
            NewGame(); 
        }


        private void NewGame()
        {
            List<Button> buttons = Container.Children.Cast<Button>().ToList();
            List<Button> rowButtons;

            for (int i = 0; i < 5; i++)
            {
                rowButtons = buttons.Where(button => Grid.GetRow(button) == i).ToList();

                rowButtons.ForEach(button => {
                    button.Content = string.Empty;
                    button.Background = Brushes.DarkTurquoise;
                    button.Foreground = Brushes.DimGray;
                });
            }
            
            mResults = new MarkType[25];

            for (var i = 0; i < mResults.Length; i++)
                mResults[i] = MarkType.Free;

            mPlayer1Turn = true;

            mGameEnded = false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (mGameEnded)
            {
                NewGame();
                return;
            }

            var button = (Button)sender;

            var column = Grid.GetColumn(button);
            var row = Grid.GetRow(button);

            var index = column + (row * 5);

            if (mResults[index] != MarkType.Free)
                return;

            mResults[index] = mPlayer1Turn ? MarkType.Cross : MarkType.Nought;

            button.Content = mPlayer1Turn ? "X" : "O";

            if (!mPlayer1Turn)
                button.Foreground = Brushes.Moccasin;

            // Смена игрока
            mPlayer1Turn ^= true;

            CheckForWinner();
        }

        private void CheckForWinner()
        {
            List<Button> buttons = Container.Children.Cast<Button>().ToList();

            //горизонталь
            for (int i = 0; i < 5; i++)
            {
                i = 5 * i;
                if (mResults[i] != MarkType.Free && (mResults[i] & mResults[i + 1] & mResults[i + 2] & mResults[i + 3] & mResults[i + 4]) == mResults[i])
                {
                    buttons.Where(button => Grid.GetRow(button) == i/5).ToList().ForEach(button => button.Background = Brushes.MediumSpringGreen);
                    mGameEnded = true;
                    if (mResults[i] == MarkType.Cross) {
                        MessageBox.Show("X win!");
                    } 
                    else
                    {
                        MessageBox.Show("O win!");
                    }
                    return;
                }
                i = i/5;
            }


            //вертикаль
            for (int i = 0; i < 5; i++)
                {                            
                 if (mResults[i] != MarkType.Free && (mResults[i] & mResults[i + 5] & mResults[i + 10] & mResults[i + 15] & mResults[i + 20]) == mResults[i])
                 {
                    buttons.Where(button => Grid.GetColumn(button) == i).ToList().ForEach(button => button.Background = Brushes.MediumSpringGreen);
                    mGameEnded = true;
                    if (mResults[i] == MarkType.Cross)
                    {
                        MessageBox.Show("X win!");
                    }
                    else
                    {
                        MessageBox.Show("O win!");
                    }
                    return;
                 }
            }


            //диагонали
            if (mResults[0] != MarkType.Free && (mResults[0] & mResults[6] & mResults[12] & mResults[18] & mResults[24]) == mResults[0])
            {
                Button0_0.Background = Button1_1.Background = Button2_2.Background = Button3_3.Background = Button4_4.Background = Brushes.MediumSpringGreen;
                mGameEnded = true;
                if (mResults[0] == MarkType.Cross)
                {
                    MessageBox.Show("X win!");
                }
                else
                {
                    MessageBox.Show("O win!");
                }
                return;
            }

            if (mResults[4] != MarkType.Free && (mResults[4] & mResults[8] & mResults[12] & mResults[16] & mResults[20]) == mResults[4])
            {
                Button4_0.Background = Button3_1.Background = Button2_2.Background = Button1_3.Background = Button0_4.Background = Brushes.MediumSpringGreen;
                mGameEnded = true;
                if (mResults[4] == MarkType.Cross)
                {
                    MessageBox.Show("X win!");
                }
                else
                {
                    MessageBox.Show("O win!");
                }
                return;
            }

            
            //нет победы
            if (!mResults.Any(cell => cell == MarkType.Free))
            {
                mGameEnded = true;
                Container.Children.Cast<Button>().ToList().ForEach(button =>
                {
                    button.Background = Brushes.Orange;
                });
                MessageBox.Show("NO win!");
            }
        }
    }


 }

